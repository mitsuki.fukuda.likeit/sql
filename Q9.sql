select
	t1.category_name,
	sum(t.item_price) as total_price
from
	item t
inner join
	item_category t1
on
	t.category_id = t1.category_id
group by
	t1.category_name
order by
	total_price desc
;

