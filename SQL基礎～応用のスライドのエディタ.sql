use textbook_sample;

-- テーブル結合 INNER JOIN
SELECT
	t.sale_id,
	t.sale_date,
	t.cust_id,
	t1.item_id,
	t1.item_count
FROM
	t_sale t
INNER JOIN
	t_sale_detail t1
ON
	t.sale_id = t1.sale_id
WHERE
	t.sale_id = '0001'
AND
	t1.item_id = '0001';


-- LEFT OUTER JOIN
SELECT
	t.sale_id,
	t.sale_date,
	t.cust_id,
	t1.item_id,
	t1.item_count
FROM
	t_sale t
LEFT OUTER JOIN
	t_sale_detail t1
ON
	t.sale_id = t1.sale_id
WHERE
	t.sale_id = '0001'
AND
	t1.item_id = '0001';


-- 内部結合  鈴木さんが2019-05-05に買った商品名
use textbook_sample;

SELECT
	m1.*
FROM
	m_customer m
INNER JOIN
	t_sale t
ON
	m.cust_id = t.cust_id
INNER JOIN
	t_sale_detail t1
ON
	t.sale_id = t1.sale_id
INNER JOIN
	m_item m1
ON
	t1.item_id = m1.item_id
WHERE
	m.cust_nm = '鈴木'
AND
	t.sale_date = '2019-05-05';



-- 部分一致検索
SELECT
	t.*
FROM
	m_customer t
WHERE
	t.cust_nm LIKE '%鈴%';


SELECT
	t.*
FROM
	m_customer t
WHERE
	t.cust_nm LIKE '%木%';


-- 部分一致検索 ワイルドカード
SELECT
	t.*
FROM
	m_customer t
WHERE
	t.cust_nm LIKE '%佐%';


SELECT
	t.*
FROM
	m_customer t
WHERE
	t.cust_nm LIKE '田%';


SELECT
	t.*
FROM
	m_customer t
WHERE
	t.cust_nm LIKE '%木';

SELECT
	t.*
FROM
	m_customer t
WHERE
	t.cust_nm LIKE '佐_';


SELECT
	t.*
FROM
	m_customer t
WHERE
	t.cust_nm LIKE '_田';


-- ソート 昇順
SELECT
	*
FROM
	t_sale t
ORDER BY
	t.sale_date ASC;

-- ソート 降順
SELECT
	*
FROM
	t_sale t
ORDER BY
	t.sale_date DESC;


-- グルーピング
SELECT
	t.sale_id
FROM
	t_sale_detail t;

SELECT
	t.sale_id
FROM
	t_sale_detail t
GROUP BY
	sale_id ;


-- 集約関数
SELECT
	MAX(t.item_price)
FROM
	m_item t;


-- グルーピング+集約関数
-- 日付別に購入された商品の合計数
	-- 1,伝票表の日付、伝票詳細表の商品数が必要
	-- 2,伝票表・伝票詳細表の結合
	-- 3,同じ日でグルーピング
	-- 4,同じ日付のレコードをまとめて合計値SUMを算出

	-- 1 + 2
SELECT
	t.sale_date,
	t1.item_count
FROM
	t_sale t
INNER JOIN
	t_sale_detail t1
ON
	t.sale_id = t1.sale_id ;

	-- 1 + 2 + 3
SELECT
	t.sale_date,
	t1.item_count
FROM
	t_sale t
INNER JOIN
	t_sale_detail t1
ON
	t.sale_id = t1.sale_id
GROUP BY
	t.sale_date ;

	-- 1 + 2 + 3 + 4
SELECT
	t.sale_date,
	SUM(t1.item_count) AS item_sums
FROM
	t_sale t
INNER JOIN
	t_sale_detail t1
ON
	t.sale_id = t1.sale_id
GROUP BY
	t.sale_date ;



